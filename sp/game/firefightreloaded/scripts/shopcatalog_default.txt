"ShopCatalog"
{
	"1"
	{
		"name"		"HealthKit"
		"price"		"10"
		"command"	"healthkit"
	}
	"2"
	{
		"name"		"SuitBattery"
		"price"		"20"
		"command"	"battery"
	}
	"3"
	{
		"name"		"MaxHealth"
		"price"		"150"
		"command"	"upgrade 0"
	}
    "4"
    {
        "name"		"Knife"
        "price"		"450"
        "command"	"item weapon_knife 0"
    }
	"5"
	{
		"name"		"PistolAmmo"
		"price"		"10"
		"command"	"ammo Pistol 20"
	}
	"6"
	{
		"name"		"SMGAmmo"
		"price"		"15"
		"command"	"ammo SMG1 45"
	}
	"7"
	{
		"name"		"SMGGrenade"
		"price"		"85"
		"command"	"ammo SMG1_Grenade 1"
	}
	"8"
	{
		"name"		"MP5Ammo"
		"price"		"10"
		"command"	"ammo MP5Ammo 30"
	}
	"9"
	{
		"name"		"FragGrenade"
		"price"		"150"
		"command"	"item item_ammo_grenade 1"
	}
	"10"
	{
		"name"		"SLAM"
		"price"		"175"
		"command"	"item item_slam_ammo 1"
	}
	"11"
	{
		"name"		"357Magnum"
		"price"		"200"
		"command"	"item weapon_357 0"
	}
	"12"
	{
		"name"		"357Ammo"
		"price"		"40"
		"command"	"ammo 357 20"
	}
	"13"
	{
		"name"		"AR2"
		"price"		"245"
		"command"	"item weapon_ar2 0"
	}
	"14"
	{
		"name"		"AR2Ammo"
		"price"		"15"
		"command"	"ammo AR2 20"
	}
	"15"
	{
		"name"		"AR2DarkEnergyBall"
		"price"		"150"
		"command"	"ammo AR2AltFire 1"
	}
	"16"
	{
		"name"		"Crossbow"
		"price"		"255"
		"command"	"item weapon_crossbow 0"
	}
	"17"
	{
		"name"		"CrossbowAmmo"
		"price"		"55"
		"command"	"ammo XBowBolt 20"
	}
	"18"
	{
		"name"		"Shotgun"
		"price"		"250"
		"command"	"item weapon_shotgun 0"
	}
	"19"
	{
		"name"		"ShotgunAmmo"
		"price"		"25"
		"command"	"ammo Buckshot 20" 
	}
	"20"
	{
		"name"		"RPG"
		"price"		"350"
		"command"	"item weapon_rpg 0" 
	}
	"21"
	{
		"name"		"RPGAmmo"
		"price"		"150"
		"command"	"ammo RPG_Round 1" 
	}
	"22"
	{
		"name"		"SniperRifle"
		"price"		"260"
		"command"	"item weapon_sniper_rifle 0" 
	}
	"23"
	{
		"name"		"SniperRifleAmmo"
		"price"		"65"
		"command"	"ammo Sniper 15" 
	}
	"24"
	{
		"name"		"M249Para"
		"price"		"330"
		"command"	"item weapon_m249para 0" 
	}
	"25"
	{
		"name"		"M249ParaAmmo"
		"price"		"65"
		"command"	"ammo M249 120" 
	}
	"26"
	{
		"name"		"Gauss"
		"price"		"1500"
		"command"	"item weapon_gauss 0" 
	}
	"27"
	{
		"name"		"GaussAmmo"
		"price"		"200"
		"command"	"ammo GaussEnergy 30" 
	}
	"28"
	{
		"name"		"Katana"
		"price"		"3000"
		"command"	"item weapon_katana 0" 
	}
    "29"
	{
		"name"		"Railgun"
		"price"		"3500"
		"command"	"item weapon_railgun 0" 
	}
	"30"
	{
		"name"		"RailgunAmmo"
		"price"		"800"
		"command"	"ammo Railgun 25" 
	}
    "31"
	{
		"name"		"RailgunOvercharge"
		"price"		"6400"
		"command"	"ammo Railgun 200" 
	}
}